const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

router.post("/register", (request, response) => {

    if(request.body.firstName == "" || request.body.firstName == null || request.body.lastName == "" || request.body.lastName == null || request.body.email == "" || request.body.email == null || request.body.password == "" || request.body.password == null){
        return response.send({
            message: "All fields are required"
        })
    }

    UserController.register(request.body).then((result) => {
        response.send(result)
    })
})

router.get("/login", (request, response) => {
    UserController.login(request.body).then((result) => {
        response.send(result)
    })
})

router.get("/:id/view",auth.verify, (request, response) => {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    
    if(data.userId === request.params.id || data.isAdmin){
        UserController.getUserDetails(request.params.id).then((result) => {
            response.send(result)
        })
    } else {
        response.send({
            message: "You are not authorize to view other user's details"
        })
    }  
})

router.get("/", auth.verify, (request, response) => {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    
    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.getAllUsers().then((result) => {
        response.send(result)
    })
    
})

router.patch("/:id/change-usertype", auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.changeUserTypetoAdmin(request.params.id).then((result) => {
        response.send(result)
    })
})

router.patch("/:id/assign-csr", auth.verify, (request, response)=> {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.assignUserToCsr(request.params.id).then((result)=> {
        return response.send(result)
    })
})



router.get("/cart", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    UserController.viewCart(data).then((result)=> {
        return response.send(result)
    })

})

router.patch("/cart/:id/change-quantity/:quantity", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        productId: request.params.id,
        quantity: Number(request.params.quantity),
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    UserController.changeProductQuantity(data).then((result)=> {
        response.send(result)
    })
})

router.patch("/cart/:id/remove-item", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        productId: request.params.id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    UserController.removeItemFromTheCart(data).then((result)=> {
        response.send(result)
    })
})

router.post("/check-out", auth.verify, (request, response) => {


    const data = {
		userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    UserController.checkOut(data).then((result) => {
        response.send(result)
    })
})

module.exports = router