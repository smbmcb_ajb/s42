const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const MessageController = require('../controllers/MessageController')
const auth = require('../auth')

router.post("/send-message/:id", auth.verify, ((request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        receiverId: request.params.id,
        message:request.body.message
    }
    MessageController.sendMessage(data).then((result)=> {
        response.send(result)
    })
})
)

module.exports = router