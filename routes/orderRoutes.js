const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')




router.get("/history/:status", auth.verify, (request, response) => {

    const data = {
		userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        status: request.params.status
	}

    if(data.isAdmin){
        return response.send({message: "This is not a page for admin."})
    }

    OrderController.viewHistoryByStatus(data).then((result)=> {
        return response.send(result)
    })
})

router.get("/history", auth.verify, (request, response) => {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "This is not a page for admin."})
    }

    OrderController.getAllOrderHistory(data).then((result) => {
        
        response.send(result)
    }) 
})

router.get("/admin/view/:status", auth.verify, (request, response) => {

    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        status: request.params.status
	}

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    OrderController.viewOrders(data).then((result)=> {
        return response.send(result)
    })
})

router.patch("/admin/update/:status/:id", auth.verify, (request, response) => {

    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        status: request.params.status,
        orderId: request.params.id
	}

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    OrderController.updateOrderStatus(data).then((result)=> {
        return response.send(result)
    })
})
module.exports = router