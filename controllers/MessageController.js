const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
const User = require('../models/User')
const Message = require('../models/Message')

module.exports.sendMessage = (data)=> {
    let senderName, receiverName
    User.findById(data.userId).then((result)=> {
        senderName = result.firstName
    })

    User.findById(data.receiverId).then((result)=> {
        receiverName = result.firstName
    })

    let myMessage = [], receiverMessage = []
    let new_message = new Message(
        
            {
                fromUser: data.userId,
                toUser: data.receiverId,
                messages: data.message
            }
        
        
    )

    return new_message.save().then((newMessage, error)=> {
        if(error){
            return {
                message: "Something went wrong."
            }
        }
        // sender = newMessage.userId
        // receiver = newMessage.receiverId
        return newMessage
    })
    .then((result)=> {
        return Message.find().then((result)=> {
            console.log(result.conversation)
            result.map((conversations)=> {
                if(data.userId == conversations.fromUser && data.receiverId == conversations.toUser){
                    myMessage.push(`${senderName}: ${conversations.messages}`)
                }

                if(data.userId == conversations.toUser && data.receiverId == conversations.fromUser){
                    myMessage.push(`${receiverName}: ${conversations.messages}`)
                }
            })
            return myMessage
        })
        })
    
}