const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
const User = require('../models/User')
// const Cart = require('../models/Cart')


module.exports.addProduct = (data) => {
    

		let new_product = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return new_product.save().then((new_product, error) => {
				if(error) {
					return false
				}

				return {
					message: `New product ${data.product.name} successfully created!`
				}
		})
	

	// let message = Promise.resolve("User must be ADMIN to access this")

	// 	return message.then((value) => {
	// 		return value
	// 	})
		// return Promise.resolve({
		// 	message: "User must be ADMIN to access this"
		// })
}

module.exports.getAllProducts = (data) => {
	if(data == "" || data == null){
		return Product.find({isActive: true}).then((result) => {
			return result
		})
	}

	const access = {
		isAdmin: auth.decode(data).isAdmin
	}
    
	if(access.isAdmin){
		return Product.find().then((result) => {
			return result
		})
	}
	
	return Product.find({isActive: true}).then((result) => {
		return result
	})
	    
}

module.exports.getSpecificProduct = (data) => {
	return Product.find({_id:data}, (error, retrieved) => {
		// console.log(retrieved)
		if(error){
			return {
				message: "Product does not exist!"
			}
			
		}
	}).clone().catch((error) => { 
		return {
			message: "Product does not exist!"
		}
	})
	.then((result) => {
		// console.log(result)
		return result
	})
}

module.exports.updateProduct = (product_id, new_data) => {
	return Product.findByIdAndUpdate(product_id, {
		name: new_data.name,
		description: new_data.description,
		price: new_data.price
	}, (error, retrieved) => {}).clone().catch((error) => { 
		return {
			message: "Product does not exist!"
		}
	
	}).then((result) => {
		
		if(result._id){
			return {
					message: "Product has been successfully updated!"
				}
		}
		return result
		
	})
	
}

module.exports.archiveProduct = (product_id) => {
	return Product.findByIdAndUpdate(product_id, {
		isActive: false
	}).then((updated_product, error) => {
		if(error) {
			return false
		}
		return {
			message: "Product has been archived successfully"
		}
	})
}

module.exports.unArchiveProduct = (product_id) => {
	return Product.findByIdAndUpdate(product_id, {
		isActive: true
	}).then((updated_product, error) => {
		if(error) {
			return false
		}
		return {
			message: "Product has been unarchived successfully"
		}
	})
}

module.exports.addToCart = (data) => {
	let productIdArray = []
	// let productQuantityArray = []
	let totalQuantity = 0
	let productPrice = 0
	
	return User.findById(data.userId).then((result) => {
		result.cart.products.map((item) => {
			productIdArray.push(item.productId)
			
			if(item.productId === data.productId){
				totalQuantity = item.quantity + data.quantity
				productPrice = item.price
				
			}
			
		})
		
		let found 
		return found = productIdArray.find((product) => {
			return product == data.productId
			
		})
		
	}).then((result) => {
		if(result === undefined){
			return Product.findById(data.productId).then((result) => {
				return {
					productId:result.id,
					name: result.name,
					price: result.price,
					quantity: data.quantity,
					subTotal: result.price * data.quantity
				}	
			})
			.then((result) => {
				
				return User.findById(data.userId).then((user) => {
					let total = 0
					user.cart.products.push(result)
					
					user.cart.products.map((product) => {
						total = total + product.subTotal
						// console.log(total)
					})
					user.cart.totalAmount = total
					// console.log(total)
					return user.save().then((addedToCart, error) => {
						if(error){
							return false
						}
						return {
							message: "Added to cart successfully!"
						}
					})
					
				})
				
			})
		}else {
			return User.updateOne(
				{
					$and: [
						{
							"cart.products": {"$elemMatch":{"productId": data.productId}},
						},
						{_id: data.userId}
					]
				},
		
				
					
				{
					"$set": {"cart.products.$.quantity":totalQuantity, "cart.products.$.subTotal": totalQuantity * productPrice}
				}
				
				).then((result) => {
					let allSubTotal = 0
					return User.findById(data.userId).then((result)=> {
						result.cart.products.map((item)=> {
							allSubTotal += item.subTotal 
						})
						// console.log(result)
						result.cart.totalAmount = allSubTotal

						return result.save().then((totalAmountUpdated, error)=> {
							if(error){
								return false
							}

							return {
								message: "Added to cart successfully"
							}
						})
						
					})
					
			})
		}	
	}).then((result)=> {
		return result
	})
	
}



module.exports.testChangeDataInsideArray = (data) => {
	return User.updateOne(
		
		
		{
			"cart.products": {"$elemMatch":{"productId": "633fe0d94d944930ae12b2ce"}}
		},
			
		{
		"$set": {"cart.products.$.quantity":10}
		}
		
		

		).then((result) => {
			console.log(data)
		console.log
	})
}