const User = require('../models/User')
// const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
// const { response } = require('express')
const Order = require('../models/Order')
const Product = require('../models/Product')
// const { findById } = require('../models/Product')


module.exports.register = (data) => {
    let encrypted_password = bcrypt.hashSync(data.password, 10)
    
    let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		password: encrypted_password
	})

    return new_user.save().then((created_user, error) => {
        if(error){
            return false
        }
        return {
            message: `User ${new_user.firstName} Successfully registered`
        }
    })
}

module.exports.login = (data) => {
    
    return User.findOne({email: data.email}).then((result) => {
        
        if(result == null){
            return {
                message: "User does not exist!"
            }
        }
        
        const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
            let messages
            if(result.isAdmin){
                messages = `Hello there! Admin ${result.firstName}.`
            }
            else {
                messages = `Hello there! ${result.firstName}.`
            }

			return {
				accessToken:auth.createAccessToken(result),
                message: messages
			}
		}

		return {
			message: 'Password is incorrect!'
		}
    })
}

module.exports.getUserDetails = (user_id) => {
    return User.findById({_id: user_id},{password:0}, (error, retrieved) => {}).clone().catch((error) => { 
		return {
			message: "User does not exist!"
		}
	})
	.then((result) => {
		
		return result
	})
}

module.exports.getAllUsers = () => {
    return User.find({},{password:0}).then((result) => {
        return result
    })
}

module.exports.changeUserTypetoAdmin = (user_id) => {
    return User.findByIdAndUpdate(user_id, {isAdmin: true}, (error, result) => {}).clone().catch((error) => {
        return {
            message: "User does not exist!"
        }
    })
    .then((result) => {
        return User.findById({_id:result.id},{firstName:1, lastName:1, email:1,isAdmin:1})
    })
    .then((result) => {
        return {
            message: `User ${result.firstName}'s user type has been changed to admin`,
            details: result
        }
    })
}

module.exports.assignUserToCsr = (user_id) => {
    return User.findByIdAndUpdate({_id:user_id}, {"isCustomerService": true}, (error, result)=> {}).clone().catch((error)=> {
        return{
            message: "Invalid User ID!"
        }
    })
    .then((result)=> {
        if(result._id){
            return {
                message: `${result.firstName} has been successfully assigned as CSR.`
            }
        }
        return result
    })
}



module.exports.viewCart = (user_id) => {
    return User.findById(user_id.userId,{cart:1}).then((result)=> {
        return result
    })
}

module.exports.changeProductQuantity = (data)=> {
    return Product.findById(data.productId, (resolve,error)=>{}).clone().catch((error)=> {
        return {
            message: "Unkown product Id"
        }
    })
    .then((result)=> {
        // console.log(result)
        return result
    }).then((result)=> {
        if(result == "" | result == null){
            // console.log(result)
            return {
                message: "Unkown product Id"
            }
        }
        if(result._id){
            return User.updateOne
            (
                {
                    $and: [
                        {
                            "cart.products": {"$elemMatch":{"productId": data.productId}},
                        },
                        {_id: data.userId}
                    ]
                },    
                {
                    "$set": {"cart.products.$.quantity":data.quantity, "cart.products.$.subTotal": data.quantity * result.price}
                }
            ).then((result)=> {
                if(result.modifiedCount){
                    return User.findById(data.userId).then((result)=> {
                        let total=0
                        result.cart.products.map((item)=> {
                            total += item.subTotal
                        })
                        
                        result.cart.totalAmount = total
                        return result.save().then((updated_total,error)=> {
                            if(error){
                                return{
                                    message: "An error occured!"
                                }
                            }
                            return {
                                message: "Product quantity has been successfully changed!"
                            }
                        })
                    })
                }
            if(result.matchedCount){
                return {
                    message: "Your desired quantity is the same as the quantity inside your cart. Change is not required"
                }
            }
            return {
                message: "Unable to update quantity. Product is not yet included in your shopping cart!"
        } 
    })
    }
        
        return result
    })
    
        
    
}

module.exports.removeItemFromTheCart = (data)=> {
    let message
    return User.findById(data.userId)
    .then((result)=> {
        for(let i = 0; i < result.cart.products.length; i++){
            if(data.productId == result.cart.products[i].productId){
                result.cart.totalAmount = result.cart.totalAmount - result.cart.products[i].subTotal
                message = `${result.cart.products[i].name} has been successfully removed from your shopping cart.`
                result.cart.products.splice(i, 1)
                
            }
        }
        return result.save().then((success, error)=> {
            if(error){
                return {
                    message: "Something went wrong."
                }
            }

            if(message){
                return {
                    message: message
                }
            }

            return {
                message: "Product does not exist in your shopping cart."
            }
        })
    })
}

module.exports.checkOut = async (data) =>{
    let isOderUpdated = await User.findById(data.userId).then((result)=> {
        let new_order = new Order({
            userId: result._id,
            name: `${result.firstName} ${result.lastName}`,
            order: result.cart.products,
            totalAmount: result.cart.totalAmount
        })
        
        return new_order.save().then((success, error)=>{
            if(error){
                return false
            }

            // console.log(success)
            return true
        }) 
    })
    
    let isCartIsCleared = await User.findById(data.userId).then((result)=> {
        result.cart.products = []
        result.cart.totalAmount = 0
        return result.save().then((success, error)=>{
            if(error){
                return false
            }
            return true
        }) 

        
    })

    if(isOderUpdated && isCartIsCleared){
        return {
            message: "Order has been placed. Please wait 2-3 business days for the delivery of your order/s"
        }
    }
    return {
        message: "Something went wrong."
    }
}
