const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isCustomerService: {
		type: Boolean
	},
	cart: {
		products:[
		{
			productId: 
			{
				type: String,
				required: [true, 'Product id is Required.']
			},

			name: 
			{
				type: String,
				required: [true, "Name is required."]
			},
			
			price: 
			{
				type: Number,
				required:[true, 'Price is required']
			},

			quantity: 
			{
				type: Number,
				required:[true, 'Quantity is required']
			},

			subTotal: 
			{
				type: Number,
				required:[true, 'Sub-total is required']
			}
		}
		
		],
		totalAmount:
		{
			type: Number,
			required:[true, 'Total amount is required'],
			default:0
		}
	},
	registeredOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('User', user_schema)