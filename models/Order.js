const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
	userId: 
			{
				type: String,
				required: [true, 'User id is Required.']
			},

	name: 
			{
				type: String,
				required: [true, "Name is required."]
			},
    order:[
		{
			productId: 
			{
				type: String,
				required: [true, 'Product id is Required.']
			},

			name: 
			{
				type: String,
				required: [true, "Name is required."]
			},
			
			price: 
			{
				type: Number,
				required:[true, 'Price is required']
			},

			quantity: 
			{
				type: Number,
				required:[true, 'Quantity is required']
			},

			subTotal: 
			{
				type: Number,
				required:[true, 'Sub-total is required']
			}
		}
		
		],
		totalAmount:
		{
			type: Number,
			required:[true, 'Total amount is required'],
			default:0
		},
        status: 
			{
				type: String,
				required: [true, 'Product id is Required.'],
                default:"pending"
			},
        createdOn: {
            type: Date,
            default: new Date()
        }
        

})

module.exports = mongoose.model('Order', order_schema)